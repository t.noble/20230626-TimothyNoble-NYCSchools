package com.example.jpmc_challenge.repository

import com.example.jpmc_challenge.data.NycSchools
import com.example.jpmc_challenge.data.NycSchoolsDAO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.URL

class NycSchoolsRepository(private val nycSchoolsDao : NycSchoolsDAO) {
    suspend fun fetchAndInsertSchoolsFromUrl(url: String) {
        withContext(Dispatchers.IO) {
            val schools = fetchDataFromUrl(url)
            nycSchoolsDao.insertSchools(schools)
        }
    }

    private fun fetchDataFromUrl(url: String): List<NycSchools> {
        val jsonData = URL(url).readText()
        // Parse the JSON data and create a list of School objects
        // Replace this with your own logic based on the structure of the JSON data
        val schools = parseJsonData(jsonData)
        return schools
    }

    private fun parseJsonData(jsonData: String): List<NycSchools> {
        //Implement logic to parse the JSON data
        return emptyList()
    }

    suspend fun getAllSchools(): List<NycSchools> {
        return withContext(Dispatchers.IO) {
            nycSchoolsDao.getAllSchools()
        }
    }
}