package com.example.jpmc_challenge

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.example.jpmc_challenge.data.NycSchoolsDatabase
import com.example.jpmc_challenge.model.SchoolViewModel
import com.example.jpmc_challenge.model.SchoolViewModelFactory
import com.example.jpmc_challenge.repository.NycSchoolsRepository
import com.example.jpmc_challenge.ui.theme.JPMCCHALLENGETheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var schoolViewModel: SchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val schoolRepository = NycSchoolsRepository(NycSchoolsDatabase.getInstance(this).nycSchoolsDao())
        schoolViewModel = ViewModelProvider(this, SchoolViewModelFactory(schoolRepository)).get(SchoolViewModel::class.java)

        setContent {
            JPMCCHALLENGETheme {
                val schools by schoolViewModel.schools
                SchoolList(schools)

            }
        }

        GlobalScope.launch(Dispatchers.Main) {
            schoolViewModel.fetchSchoolsFromUrl("https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        }
    }
}

@Composable
fun SchoolList (schools: List<String>) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(schools) { school ->
            Text(
                text = school,
                modifier = Modifier.padding(16.dp)
            )
        }
    }
}



