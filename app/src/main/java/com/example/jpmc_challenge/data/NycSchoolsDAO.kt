package com.example.jpmc_challenge.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NycSchoolsDAO {
    @Query("SELECT * FROM schools")
    fun getAllSchools(): List<NycSchools>

    @Insert
    suspend fun insertSchool(school: NycSchools)

    @Insert
    suspend fun insertSchools(schools: List<NycSchools>)
}