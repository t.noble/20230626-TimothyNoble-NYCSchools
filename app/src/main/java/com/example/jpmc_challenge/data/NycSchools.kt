package com.example.jpmc_challenge.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "schools")
data class NycSchools(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String
)
