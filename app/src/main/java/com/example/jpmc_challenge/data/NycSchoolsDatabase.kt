package com.example.jpmc_challenge.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [NycSchools::class], version = 1)
abstract class NycSchoolsDatabase : RoomDatabase() {
    abstract fun nycSchoolsDao(): NycSchoolsDAO

    companion object {
        private const val DATABASE_NAME = "nyc_schools"

        @Volatile
        private var instance: NycSchoolsDatabase? = null

        fun getInstance(context: Context): NycSchoolsDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): NycSchoolsDatabase {
            return Room.databaseBuilder(
                context,
                NycSchoolsDatabase::class.java,
                DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}