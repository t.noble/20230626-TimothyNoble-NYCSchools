package com.example.jpmc_challenge.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.jpmc_challenge.data.NycSchoolsDAO
import com.example.jpmc_challenge.repository.NycSchoolsRepository

class SchoolViewModelFactory(private val schoolRepository: NycSchoolsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SchoolViewModel(schoolRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}