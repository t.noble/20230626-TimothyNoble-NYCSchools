package com.example.jpmc_challenge.model

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jpmc_challenge.repository.NycSchoolsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SchoolViewModel(private val schoolRepository: NycSchoolsRepository) : ViewModel() {
    var schools by mutableStateOf<List<String>>(emptyList())

    fun fetchSchoolsFromUrl(url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            schoolRepository.fetchAndInsertSchoolsFromUrl(url)
            loadSchools()
        }
    }

    private fun loadSchools() {
        viewModelScope.launch(Dispatchers.IO) {
            val schoolList = schoolRepository.getAllSchools()
            schools = schoolList.map { it.name }
        }
    }
}